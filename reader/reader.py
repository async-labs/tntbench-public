import asyncio
import argparse
import time
import math

from aiohttp import ClientSession, TCPConnector


class Reader(object):

    def __init__(self):
        self.success_count = 0
        self.error_count = 0
        self.min_duration = None
        self.max_duration = None
        self.avg_duration = None

    async def request(self, ses, url):
        start_time = time.time()
        resp = await ses.get(url)
        elapsed = time.time() - start_time

        if resp.status == 200:
            self.success_count += 1
        else:
            self.error_count += 1

        if self.min_duration is None:
            self.min_duration = elapsed
        elif elapsed < self.min_duration:
            self.min_duration = elapsed

        if self.max_duration is None:
            self.max_duration = elapsed
        elif elapsed > self.max_duration:
            self.max_duration = elapsed

        if self.avg_duration is None:
            self.avg_duration = elapsed
        else:
            d = ((elapsed - self.avg_duration) /
                  (self.success_count + self.error_count))
            self.avg_duration = self.avg_duration + d

        print("{}s".format(elapsed))


    async def run(self, concurrency, number, url):
        connector = TCPConnector(limit=0)
        async with ClientSession(connector=connector) as ses:
            count = math.floor(number / concurrency)
            for i in range(count):
                tasks = [self.request(ses, url) for _ in range(concurrency)]
                await asyncio.wait(tasks)

            tail = number % concurrency
            if tail != 0:
                tasks = [self.request(ses, url) for _ in range(tail)]
                await asyncio.wait(tasks)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', type=int, default=1)
    parser.add_argument('-n', type=int, default=1)
    parser.add_argument('-url', type=str, default='http://127.0.0.1:8080/read?limit=50')
    args = parser.parse_args()

    reader = Reader()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(reader.run(args.c, args.n, args.url))

    print("Success request: {}".format(reader.success_count))
    print("Failed request: {}".format(reader.error_count))
    print("Minimum duration: {}s".format(reader.min_duration))
    print("Maximum duration: {}s".format(reader.max_duration))
    print("Average duration: {}s".format(reader.avg_duration))


if __name__ == '__main__':
    main()
