#!/usr/bin/env bash
set -e

# Install requierements for Python
apt-get update
apt-get install -y gcc make zlib1g-dev libssl-dev

# Make temporary dir
PYTHON_BUILD_DIR="/tmp/python"
mkdir -p $PYTHON_BUILD_DIR
cd $PYTHON_BUILD_DIR

# Download and extract Python source code
PYTHON_VERSION="3.6.1"
wget http://www.python.org/ftp/python/$PYTHON_VERSION/Python-$PYTHON_VERSION.tar.xz
tar -xvf Python-$PYTHON_VERSION.tar.xz

# Configure, build and install Python
cd Python-$PYTHON_VERSION
./configure --prefix=/usr/local
make
make install

# Add /usr/local/bin ahead of /usr/bin
export PATH="/usr/local/bin:$PATH"
echo 'export PATH="/usr/local/bin:$PATH"' >> /home/ubuntu/.bashrc

# Install pip
curl -sL https://bootstrap.pypa.io/get-pip.py | python3.6

# Install requirements for Tarantool
apt-get -y install apt-transport-https lsb

# Register repository with Tarantool
OS_VERSION=`lsb_release -c -s`
curl http://download.tarantool.org/tarantool/1.8/gpgkey | apt-key add -
rm -f /etc/apt/sources.list.d/*tarantool*.list
tee /etc/apt/sources.list.d/tarantool_1_8.list <<- EOF
deb http://download.tarantool.org/tarantool/1.8/ubuntu/ $OS_VERSION main
deb-src http://download.tarantool.org/tarantool/1.8/ubuntu/ $OS_VERSION main
EOF

# Install Tarantool
apt-get update
apt-get -y install tarantool

# Install nodejs
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nodejs

# Install yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
apt-get update
apt-get -y install yarn

# Auto cd to sources dir
echo 'cd ~/tntbench' >> /home/ubuntu/.bashrc

# Install deps for python server and frontend
cd /home/ubuntu/tntbench
make install-requirements
cd front && yarn && cd ..
