local fiber = require('fiber')
local log = require('log')

local DIR = os.getenv('TNTBENCH_DBDIR') or '.db'
local CLEAN_SLEEP = tonumber(os.getenv('TNTBENCH_CLEAN_SLEEP')) or 5
local MAX_TUPLES_COUNT = tonumber(os.getenv('TNTBENCH_MAX_TUPLES_COUNT')) or 50000

print(CLEAN_SLEEP, MAX_TUPLES_COUNT)

pcall(function ()
  os.execute('mkdir ' .. DIR)
end)

box.cfg{
  listen = 3303,
  work_dir = DIR,
}

-- migrations

local function migration001()
  local space = box.schema.space.create('tntbench')

  space:create_index('primary', { parts={1, 'integer', 2, 'integer'} })
  space:create_index('time', { unique=false, parts={ 3, 'integer' } })
  space:create_index('int', { unique=false, parts={ 4, 'integer' } })

  box.schema.user.create('tntbench', {password='tntbench'})
  box.schema.user.grant('tntbench', 'read,write,execute', 'universe')
end

-- bootstrap
box.once('migration001', migration001)

-- cleaner fiber
local function clean()
  local space = box.space.tntbench
  local time_index = space.index.time
  local count_all = time_index:count(nil, { iterator='ALL' })

  if count_all <= MAX_TUPLES_COUNT then
    return 0
  end

  local todelete = count_all - MAX_TUPLES_COUNT
  local deleted = 0

  for _, tuple in time_index:pairs(0, { iterator='GT' }) do
    space:delete{tuple[1], tuple[2]}
    deleted = deleted + 1
    if deleted >= todelete then
      break
    end
  end

  return todelete
end

local function clean_loop()
  while true do
    log.info('clean started')
    local deleted = clean()
    log.info('deleted ' .. deleted .. ' tuples')

    fiber.sleep(CLEAN_SLEEP)
  end
end

fiber.create(clean_loop)

-- stored procedures
local counter = box.space.tntbench:count()

function add(uuid1, uuid2, int, float, str200, str20)
  box.space.tntbench:insert{uuid1, uuid2, fiber.time64(), int, float, str200, str20}
  counter = counter + 1
end

function get_counter()
  return counter
end
