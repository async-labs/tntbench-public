
develop: install-requirements
	pip3 install ipython ipdb

install-requirements:
	pip3 install -r requirements.txt -U

run-server:
	python3 server/server.py

run-writer:
	python3 writer/writer.py

run-reader:
	python3 reader/reader.py

initdb:
	tarantool db.lua &

resetdb:
	rm -r .db

.PHONY: install-requirement develop run-server run-writer run-reader initdb resetdb
