import argparse
import asyncio
import logging
import uuid
import sys

from aiohttp import web
import aiotarantool
from tarantool.const import ITERATOR_LE, ITERATOR_REQ


STATS_TIMEOUT = 5

logger = logging.Logger('aiohttp.access')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())


class Handler(object):

    def __init__(self, tnt, space):
        self.tnt = tnt
        self.space = space

    async def write(self, request):
        uid = uuid.uuid4()
        data = await request.json()
        await self.tnt.call('add', (
            int(uid.hex[:16], 16),
            int(uid.hex[16:], 16),
            data.get('int'),
            data.get('float'),
            data.get('string200'),
            data.get('string20'),
        ))
        return web.Response()

    async def read(self, request):
        offset = int(request.query.get('offset', 0))
        offset = max(offset, 0)
        limit = int(request.query.get('limit', 0))
        limit = min(max(limit, 0), 100)

        f_int = request.query.get('int')
        if f_int is not None:
            tnt_resp = await self._read_by_int(offset, limit, int(f_int))
        else:
            # read by time by default
            tnt_resp = await self._read_by_time(offset, limit)

        return web.json_response(tnt_resp.data)

    async def _read_by_time(self, offset, limit):
        return await self.tnt.select(
            self.space,
            sys.maxsize,
            index='time',
            iterator=ITERATOR_LE,
            offset=offset,
            limit=limit)

    async def _read_by_int(self, offset, limit, key):
        return await self.tnt.select(
            self.space,
            key,
            index='int',
            iterator=ITERATOR_REQ, # from newest to oldest
            offset=offset,
            limit=limit)

    async def listen(self, request):
        ws = web.WebSocketResponse()
        await ws.prepare(request)

        while True:
            cnt_resp = await self.tnt.call('get_counter')
            await ws.send_json({
                'type': 'stats',
                'data': {'count': cnt_resp[0][0]},
            })
            await asyncio.sleep(STATS_TIMEOUT)

        return ws


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-tnt-space', type=str, default='tntbench')
    parser.add_argument('-tnt-host', type=str, default='127.0.0.1')
    parser.add_argument('-tnt-port', type=int, default=3303)
    parser.add_argument('-tnt-user', type=str, default='tntbench')
    parser.add_argument('-tnt-password', type=str, default='tntbench')
    parser.add_argument('-listen-host', type=str, default='0.0.0.0')
    parser.add_argument('-listen-port', type=int, default=8080)
    args = parser.parse_args()

    tnt = aiotarantool.connect(args.tnt_host, args.tnt_port, args.tnt_user, args.tnt_password)
    handler = Handler(tnt, args.tnt_space)

    app = web.Application()
    app.router.add_route('POST', '/write', handler.write)
    app.router.add_route('GET', '/read', handler.read)
    app.router.add_route('GET', '/listen', handler.listen)
    web.run_app(app, host=args.listen_host, port=args.listen_port, access_log=logger)

if __name__ == '__main__':
    main()
