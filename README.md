Технические особенности
=======================
* Server: Python3 + aiohttp (server) + aiotarantool + tarantool
* Reader/Writer: Python3 + aiohttp (client)

Tarantool
---------
* все данные лежат в tntbench space
* формат кортежа: number(pk first), number (pk second), number(time), number (int), number, string, string
* 3 индекса - primary (два поля по 64bit), time - по времени, int - по int
* один файбер - раз в N секунд удаляет самые старые данные, если кортежей больше допустимого количества
* допустимое количество кортежей и интервал очистки настраивается через переменные окружения

Vagrant
-------
Для запуска vagrant выполнить следущие команды в корневой директории проекта:
* vagrant up

Запуск приложения:
-----------------
Для запуска/отображения логов необходимы 3 терминала (с помощью `vagrant ssh` на машине хосте):
* `make initdb` - запуск тарантула
* `make run-server` - python сервер
* `cd front && yarn start` - фронтовый сервер

Makefile
--------
Далее для установки необходимых зависимостей нужно выполнить,
находясь в корневой директории проекта в vagrant:
* sudo make install-requirements

Чтобы создать необходимый для тестирования space в tarantool, нужно выполнить,
находясь в корневой директории проекта в vagrant:
* make initdb

Для запуска web сервера (8080 порт) нужно выполнить,
находясь в корневой директории проекта в vagrant:
* make run-server

Для запуска writer-а нужно выполнить,
находясь в корневой директории проекта в vagrant:
* make run-writer

Для запуска reader-а нужно выполнить,
находясь в корневой директории проекта в vagrant:
* make run-reader

reader и writer могут быть вызваны с различными значениями, описанных ниже флагов:
* -c - максимальное количество параллельно отправляемых запросов
* -n - общее количество запросов, которое нужно отправить
* -url - запрашиваемый URL

Примеры:
* python3 writer/writer.py -c 10 -n 100000
* python3 reader/reader.py -c 10 -n 100000 -url http://127.0.0.1:8080/read?limit=50&offset=100
* python3 reader/reader.py -c 10 -n 100000 -url http://127.0.0.1:8080/read?limit=50&int=12345

Frontend
--------
На базе [create-react-app](https://github.com/facebookincubator/create-react-app)
* папка front
* `yarn` - для установки зависимостей
* `yarn start` - запуск дев-сервера (3000 порт)

Следущие шаги:
=============
- либо генерации uuid внутри tarantool ([uuid модуль](https://tarantool.org/doc/1.8/reference/reference_lua/uuid.html)), либо primary key на подобии mongodb [objectid](https://docs.mongodb.com/manual/reference/method/ObjectId/)
- настроить [memtx_memory](https://tarantool.org/doc/1.8/reference/configuration/index.html#configuring-the-storage)
- обработка ER_MEMORY_ISSUE и принудительная очистка снаружи
- [uvloop](https://github.com/MagicStack/uvloop)
- better logging for aiohttp
- пагинации по id + limit для /read (если понадобиться выводить на интерфейс)
- Application server (Gunicorn, Wsgi, ...)
