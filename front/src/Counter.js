import * as React from 'react'

import { Col } from 'react-bootstrap';
import CountTo from 'react-count-to';

export const Counter = (props) =>
  <Col xs={3} className="animated infinite pulse">
    <h2 className="counterValue">
      {
        props.interval ?
          <CountTo from={props.from} to={props.to} key={props.from + '_' + props.to} speed={props.interval} /> :
          props.to
      }
    </h2>
    <span className="title">{props.label}</span>
  </Col>
