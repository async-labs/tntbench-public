export function ws(url, onmessage) {
  let socket = new WebSocket(url);

  socket.addEventListener('message', (e) => {
    let msg;
    try {
      msg = JSON.parse(e.data);
    } catch (e) {}

    if (msg) {
      onmessage(msg);
    }
  });

  // socket.addEventListener('close')
  // socket.addEventListener('error')

  socket.addEventListener('open', () => {
    socket.send('ping');
  });
}
