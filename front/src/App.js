import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

import { ws } from './ws';
import { Counter } from './Counter';

import './App.css';


class App extends Component {

  state = {
    totalOld: 0,
    totalNew: 0,
  };

  constructor(props, context) {
    super(props, context);

    ws('ws://localhost:3000/listen', this.onMessage);
  }

  render() {
    const { totalNew, totalOld } = this.state;
    return (
      <div className="App">
        <Grid className="dataBox">
          <Row>
            <Col xs={12}><h1>Inserts:</h1></Col>
          </Row>
          <Row>
            <Col xs={3} />
            <Counter from={totalOld} to={totalNew} label={'Total count'} interval={5000} />
            <Counter from={0} to={totalNew - totalOld} label={'In the last 5 seconds'} interval={0} />
          </Row>
        </Grid>
      </div>
    );
  }

  onMessage = (msg) => {
    switch (msg.type) {
      case 'stats':
        const { totalNew, totalOld } = this.state;
        this.setState({
          totalOld: totalOld ? totalNew : msg.data.count,
          totalNew: msg.data.count,
        });
        break;
      default:
        console.log('unknown msg type');
        break;
    }
  }
}

export default App;
